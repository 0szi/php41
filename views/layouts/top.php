<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Passwortmanager 2.0</title>

    <link rel="shortcut icon" href="css/favicon.ico" type="image/x-icon">
    <link rel="icon" href="css/favicon.ico" type="image/x-icon">

    <link href="/php41/css/bootstrap.min.css" rel="stylesheet">
    <script src="/php41/js/jquery.min.js"></script>
    <script src="/php41/js/bootstrap.min.js"></script>
    <script src="/php41/js/index.js"></script>
</head>
<body>